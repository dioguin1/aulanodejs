const express = require('express');
const app = express();
const path = require('path');
const port = 3000;

app.use(express.static('public'));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/public/index.html'));
});

app.get('/objetivo', (req, res) => {
  res.sendFile(path.join(__dirname, '/public/objetivo.html'));
});

app.get('/desenvolvimento', (req, res) => {
  res.sendFile(path.join(__dirname, '/public/desenvolvimento.html'));
});

app.get('/conclusao', (req, res) => {
  res.sendFile(path.join(__dirname, '/public/conclusao.html'));
});

app.listen(port, () => {
  console.log(`Servidor rodando em http://localhost:${port}`);
});
